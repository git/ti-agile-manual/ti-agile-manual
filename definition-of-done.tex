%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                                       %%
%% Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com %%
%%                                                                       %%
%% Author: Felipe Balbi <balbi@ti.com>                                   %%
%% Author: Chase Maupin <chase.maupin@ti.com>                            %%
%%                                                                       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Definition of Done}
\label{chap:definition-of-done}

\paragraph{}
In \textit{Scrum} the defininition of "Done" is a very important concept.  As
was discussed in chapter \ref{chap:the-sprint} the goal of Scrum is to
deliver a \textbf{working} increment at the end of each sprint.  This
means that the backlog items commited in a given sprint need to be "Done"
for that item to be working.

\paragraph{}
In section \ref{sec:why-common-def} we will discuss why we need a common
definition of Done and how this differs from the acceptance criteria.  In
section \ref{sec:lcpd-definition} we will cover the LCPD definition of done
with subsections to detail each piece.  Finally in section \ref{sec:upstreaming}
we will discuss how Upstreaming of development work fits into the definition
of Done.

\section{Why a Common Definition is Important}
\label{sec:why-common-def}

\paragraph{}
A great example of why there should be a definition of Done agreed to by all
parties is found in the movie \textit{"The Great Outdoors"}.  In this movie
the main character enters a local eating challenge to eat a 96-ounce steak
in order to win a free dinner for the family.  As the scene progresses you
watch him go from confident and enjoying himself to miserable and nauseous.
It becomes apparent that he has taken on a difficult task and at the very
end when he puts the last bite of steak in his mouth you can see that he
is relieved to be finished.  At this point he declares himself Done.  That is
where the real trouble starts because the owner of the steakhouse points to
the fat and gristle left over and says he hasn't eaten everything and is not
Done.  Clearly these two people had a different definition of Done and are now
in an awkward position.  The steak has already been eaten and can't be un-eaten.
The work that was put into both preparing and eating the steak is wasted and
someone is going to have to pay the bill.

\paragraph{}
It would have been better in the preceeding example if both had agreed to
what Done meant.  Similarly we need to have an agreement within LCPD of
what we mean when we say something is done prior to starting our work.  This
will help us to avoid situations where we have conflicting expectations and
avoid wasting work.  It also helps us in planning to make sure that we are
allocating enough time to accomplish an item because we agree on what is
required to mark the item as Done.

\subsection{How this Differs from Acceptance Criteria}
\label{sec:diff-acceptance-criteria}

\paragraph{}
Often when reading about Scrum you will see discussion of \textit{Acceptance
Criteria}.  This differs from the definition of Done in that Acceptance
Criteria are created per backlog item whereas the shared definition of Done
spans all backlog items.  The Acceptance Criteria for a backlog item may be
something like:

\begin{enumerate}
    \item The driver support operating in mode x
    \item The throughput for mode x should be no less than y
    \item A functional test case has been created and delivered to the system
          test team.
    \item The Driver's User's Guide has been updated with the new feature.
\end{enumerate}

\paragraph{}
The above list differs from the shared definition of Done because it is
specific to that backlog item what is expected to be delivered for that
item for the Product Owner to accept the item as finished.

\paragraph{}
In section \ref{sec:lcpd-definition} we will see the LCPD definition of Done
and you will get a further sense of how this differs from Acceptance Criteria.

\section{LCPD Definition of Done}
\label{sec:lcpd-definition}

\paragraph{}
The purpose of this section is to outline the LCPD shared definition of Done
and give details pertaining to each part of that definition.  The LCPD
definition of Done can be broken down into the following main parts

\begin{enumerate}
    \item A Backlog item is not Done until it has been tested.  For more
	    details see section \ref{sec:lcpd-definition-tested}.
    \item A Backlog item is not Done until it has been documented.  For more
	    details see section \ref{sec:lcpd-definition-documented}.
    \item A Backlog item is not Done until it has been Submitted for
	    acceptance upstream.  For more details see section
	    \ref{sec:lcpd-definition-submitted}.
\end{enumerate}

\paragraph{}
The above components define the understanding among the members of the LCPD
group of what it means to say an item is Done and forms the foundation for our
communication of status between ourselves and other groups.

\subsection{Tested}
\label{sec:lcpd-definition-tested}

\paragraph{}
In the LCPD definition of Done an item is not considered Done until it has
been \textbf{Tested}.  Since testing can have a variety of meanings to different
people this section will detail what we mean when we say tested.

\begin{enumerate}
    \item There is a simple piece of code or scripting that will enable
          automated validation of the new functionality.
    \item Any environment setup required to run the test code is documented
          within the code itself.
    \item The developer has added the code to the automation framework so
          that it can be tested in all future kernels.  The developer can
          work with the system test team if they need assistance in adding
          their code to the automation framework.
\end{enumerate}

\paragraph{}
The goal of the tested requirement is to make sure that the delivered feature
works as expected and that the results can be reproduced by others seeking
to verify the functionality in their configuration.

\paragraph{}
This is also critical so that we can provide test reports for customers
looking to migrate from one kernel version to another.  By being able to
show that the same tests pass in the new kernel that passed in the old
kernel we can alleviate fears and ease the migration.

\subsection{Documented}
\label{sec:lcpd-definition-documented}

\paragraph{}
In the LCPD definition of Done an item is not considered Done until it has been
\textbf{Documented}.  Since documentation can take many forms the below list
will help to guide what is meant by documentation.

\begin{enumerate}
    \item The driver's user's guide, if any, has been updated to list the new
	    feature or functionality.
    \item The driver's user's guide, if any, has been updated to detail the
	    performance measurements if applicable.
    \item The driver's user's guide, if any, has been updated with usage
	    information for the new feature or functionality.
    \item Where appropriate the documentation in the source repository (i.e.
	    Kernel Documentation) has been updated.
\end{enumerate}

The purpose of the documentation requirement is to make sure that users can
easily and quickly identify what features are available, how to use them,
and the expected results.  The goal should be to document this in the most
common area possible so that the documentation can be used by others.

\subsection{Submitted}
\label{sec:lcpd-definition-submitted}

\paragraph{}
One of the most important pieces of the LCPD definition of Done is the
\textbf{Submitted} requirement.  In order to ensure that we are developing
software in a scalable and sustainable fashion, we must make sure that the
members of LCPD are pushing the development work they are doing upstream.

\paragraph{}
Without this commitment and requirement in the foundation of our work, it is
easy to reach a state where the majority of the development is done out of
tree.  This leads quickly to a technical debt that will exceed the team
capacity which, in turn, leads to either reduced feature development or
dropping support for previously developed code.

\paragraph{}
In order to avoid the accumulation of this technical debt the following
requirements have been put in place.

\begin{enumerate}
    \item For each development item there must be a submission to the upstream
	    project.  This is to enable additional code review and start the
	    upstream acceptance process.
    \item The upstream submission should be done as early in the sprint as
	    possible to allow time for feedback.
    \item When/if feedback is given on the upstream submission a new backlog
	    item should be created to address this feedback.
    \item In the rare case that a development effort cannot be submitted
	    upstream immediately a backlog item must be created to track the
	    submission requirement.
    \item When a development patch set has been submitted upstream a story
        should be created by the developer that tracks the merge of the patches
        into Linus' linux.git tree.  This item will ensure that we continue
        to track the progress of those patches as they go upstream.  Once the
        patches have been merged this item can be scheduled for the system
        test team to validate the upstream driver using the automated system.
\end{enumerate}

\paragraph{}
The above requirements ensure that the work being done by the LCPD team is
constantly being pushed upstream.  More information will be given on this,
including the role of the Product Owner in section \ref{sec:upstreaming}.


\section{Exceptions}
\label{sec:exceptions}

\paragraph{}
In the above definition of Done there may be tasks where one or more of the
items do not apply or do not make sense.  For example time spent by a
developer acting in the role of an upstream maintainer may not use the above
definition.  Another example might be time a developer spends learning about
a new part or technology.  In this case the tested and submitted parts of the
definition may not apply, but perhaps the documented section can apply by
having the developer present a brown bag to the rest of the team about the
new technology area.

\paragraph{}
Sometimes a developer may have a feature implemented but they have a
dependency on the work of another developer being submitted first, or perhaps
the maintainer is not ready for them to send the patches to them yet.

\paragraph{}
The key point here is that at any time where there is a feeling that one or
more parts of the definition of Done do not apply, this should be agreed by the
entire scrum team.  It should be discussed as part of the sprint planning and
documented in the backlog item notes to make clear what is not required and
why.  Additionally there should always be a backlog item created to track the
submission for later to make sure it does not get forgotten.

\section{Upstreaming}
\label{sec:upstreaming}

\paragraph{}
As introduced in section \ref{sec:lcpd-definition-submitted} the primary way
to keep development from adding to the technical debt of the LCPD team is to
make sure that the work being done is submitted upstream.  However, just
submitting patches to the mailing list is not sufficient.  There must be an
accurate reflection of the time required to address feedback on patches and
work with the community to gain acceptance.  The purpose of this section is
to discuss how this fits into the LCPD flow and definition of done.

\paragraph{}
Realistically it is unlikely that any particular development item will be
developed and accepted into the upstream mainline within the timebox of a
given sprint.  It is also not completely possible to predict the exact number
of iterations acceptance of a feature might take.  This leaves us with the
problem of how to track this upstream requirement working with a community
running on its own schedule while at the same time being able to develop
items and reach an agreement of Done within a sprint.  The following steps
seek to balance these two requirements.

\begin{description}
	\item[Step 1:] As discussed in section
		\ref{sec:lcpd-definition-submitted} for each development item
		there is a requirement that it be submitted to the upstream
		list.
	\item[Step 2:] For each upstream submission there should be a corresponding
		\textbf{"Merged"} backlog item created that will track when the
		code has been merged to Linus' master branch and can be tested in
        the Linux mainline.
	\item[Step 3:] If feedback is received on the upstream submission a new
		item is added to the backlog to address the feedback and create
		a new submission.  This item will be scheduled in the next
		sprint.  This process is repeated until there is no more
		feedback.  If feedback is received at the beginning of the sprint
        you should respond to the feedback immediately to acknowledge that
        it was received and if you will not be able to submit new patches
        immediately let the maintainer know when you can.
	\item[Step 4:] Once the code has been merged into the mainline master
        branch, the "Merged" backlog item created in step 2 will be submitted
        to the system test team for verification using the automated system
        that the test code for that item is now passing.
\end{description}

\paragraph{}
Using the above steps until the code is merged into the mainline there will
always be open backlog items showing the outstanding work remaining.

\subsection{Product Owner Commitment}
\label{sec:product-owner-commitment}

\paragraph{}
It is important to note that the upstreaming effort will \textbf{NOT} work
without the commitment from the product owner to prioritize the upstream
tasks above all other tasks.  The product owner must understand the following:

\begin{itemize}
	\item Once work has begun with the open source community to submit
		patches, that work must be given priority to be seen through to
		completion.  Failure to address feedback from the community in
		a timely manner will:
		\begin{itemize}
			\item Reduce credibility of developers in the open
				source community.
			\item Delay the review of future submissions of the
				patches.  After all, if the developer can't be
				bothered to respond to feedback on their
				patches why should others take time to give
				that feedback.
		\end{itemize}
	\item Any delay in submitting work to the upstream adds to the
		technical debt of the team in the following ways:
		\begin{itemize}
			\item If the architecture changes while waiting for the
				work to be upstreamed then the effort spent to
				develop that work is wasted and new effort must
				be added to the technical debt of the team to
				re-do the work based on the new architecture.
		\end{itemize}
	\item As open source merge windows approach the priority of any
		outstanding upstreams must be increased.  The impact of missing
		a merge window can be huge due to:
		\begin{itemize}
			\item The LCPD commitment for a feature to be available in a
                given kernel version means that if we do not hit that kernel
                version we must carry the patches until at least the next
                kernel version.
			\item If the merge window missed happens to be for the
				next LTS kernel then the feature will not be available on
                a long term kernel for at least a year.
		\end{itemize}
\end{itemize}

For the above reasons it is clear why the Product Owner \textbf{MUST} be
committed to upstream development and ensuring that priorities always reflect
this commitment.  While emergencies can happen that may delay an upstream
activity, it must always be with the knowledge in mind of the additional
technical debt incurred by the team.
