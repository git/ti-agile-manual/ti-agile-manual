%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                                       %%
%% Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com %%
%%                                                                       %%
%% Author: Felipe Balbi <balbi@ti.com>                                   %%
%% Author: Chase Maupin <chase.maupin@ti.com>                            %%
%%                                                                       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Estimating}
\label{chap:estimating}

\paragraph{}
A large portion of time in Scrum is spent estimating the level of effort
required for a given story.  This is obviously given so much time because
without a proper estimation of the effort required to complete a story the
scrum team is unable to commit to which items can be completed in a single
sprint.

\section{Story Points vs. Hours}
\label{sec:story-points-vs-hours}

\paragraph{}
When estimating the size and level of effort for a story it is often useful
to use \textbf{Story Points}.  Story points are a relative scale that
represents how much effort is required to complete a particular story.
Story points are expressed as relative values because it is easier for
people to rank items in relation to one another.  For example if you were
asked how many shovels of dirt and how much time it would take to dig a
20x20 foot hole 18 inches deep could you give an answer that was any better
than a wild guess?  What if I told you that digging a 20x20 foot hole 12
inches deep took 1000 shovels of dirt and 2 days.  With this information you
could make a relative estimation of the first task.  So now you could say that
it should take about 1500 shovels of dirt and 3 days.  It is much easier to
describe something as harder or easier than some known task and how much
harder or easier it seems to be.

\paragraph{}
When using a relative effort system it is usually a good idea to have
at least one item of the simplest effort.  If you have that item to
serve as a baseline of an easy story that can be completed with minimal
effort, then you now have a starting point to compare all other tasks to.
Within LCPD we currently say that a story that can be completed in a day
or less has a value of 1 story point.  This does not imply that story points
map to days.  Rather think of a story point as something between 4-8 hours of
work.  So 3 story points is about 3 times more difficult that a single story
point item.

\paragraph{}
Another reason to use story points instead of other metrics such as
hours or ideal hours is that in these cases the metric itself starts to
distract from the effort estimate.  For example if you were asked to dig
the hole about and tell roughly how many hours it would take, you might
start thinking about non-related details like whether you would need to
take time to go buy a shovel, will you need a lunch break, what about your
doctor's appointment on Tuesday?  Hours touches people at a "real" level
and instead of keeping them focused on the relative size and complexity of
the story they start worrying about things that can distract them.

\paragraph{}
Further, hours leads to a desire to be precise.  If you say a task is going
to take 60 hours you will likely feel the need to make sure you can get the
task done in exactly 60 hours.  But for a tasks that takes 60 hours, is 64
really that much more?  Is 56 that much less?  So why not give it a set of
story points that say it is a task that is \textit{about} 60 hours and
not worry about the small details.

\paragraph{}
There are other ways to estimate such as using shirt sizes like small, medium,
large, x-large.  as you will see in section \ref{sec:estimating-tool} we will
use a tool that uses a rough fibonacci sequence to allow concurrent estimation
for teams in a distributed environment.

\section{Estimating Tool}
\label{sec:estimating-tool}

\paragraph{}
One of the important things about estimating backlog items is the idea of
the simultaneous reveal of the estimation.  This is important because it
allows everyone to put out their estimation without being influenced by
the person(s) before them.  This is easy to do when everyone is in one
location but how do you do then on distributed teams where people may
be interacting with each other over the phone?

\paragraph{}
Luckily for us the fine people of "Mountain Goat Software" have an online
version of planning poker which allows the entire team to participate in
the estimation effort while still having the simultaneous reveal capability
that keeps people from having their opinions jaded by others.  This tool
can be found at \url{http://www.planningpoker.com/} and you can create
a free account.

\paragraph{}
This tool allows uploading stories, discussing them, having each team member
give an estimate of the level of effort with a simultaneous reveal.  If the
estimates do not match then the team can further discuss the item and re-vote
until a consensus is reached.  At the end of the planning session the items
and their estimates can be exported.

\section{Estimated Task Is Too Large To Fit In One Sprint}
\label{sec:estimated-task-is-too-large}

\paragraph{}
Sometime during estimation the story may be too large to fit into a single
sprint.  This may be because of some ambiguity about what the story entails,
or just that it simple is too much based on the team's velocity.  If this is
the case you should promote the story into an epic and then start working to
break this epic up into smaller stories that can be estimated to fit within
a single sprint.  For more details see chapter \ref{chap:work-breakdown}.
