PDFLATEX	?= $(shell which pdflatex)
BIBTEX		?= $(shell which bibtex)
VIEWER		?= $(shell which evince)

JOBNAME		?= "LCPD-Agile-Methodology-Manual"

OPTS		= -halt-on-error -file-line-error \
		  -interaction=batchmode \
		  -jobname=$(JOBNAME)

SOURCE		= main.tex
PDF		= $(JOBNAME)

V		= @
Q		= $(V:1=)
QUIET_BIBTEX	= $(Q:@=@echo	'      BIBTEX      '$@;)
QUIET_LATEX	= $(Q:@=@echo	'      LATEX       '$@;)
QUIET_CLEAN	= $(Q:@=@echo	'      CLEAN       '$@;)
QUIET_VIEW	= $(Q:@=@echo	'      VIEW        '$@;)

all: pdf

pdf: $(PDF)

$(PDF): $(SOURCE)
	$(QUIET_LATEX) $(PDFLATEX) $(OPTS) $<
	$(QUIET_BIBTEX) $(BIBTEX) $(JOBNAME).aux
	$(QUIET_LATEX) $(PDFLATEX) $(OPTS) $<
	$(QUIET_LATEX) $(PDFLATEX) $(OPTS) $<

view: $(PDF)
	$(QUIET_VIEW) $(VIEWER) $(PDF)

clean:
	$(QUIET_CLEAN) rm -f *.log *.aux *.lot *.lof *.toc *.tex~ *.pdf *.out *.idx *.href *.url *.bbl *.blg
