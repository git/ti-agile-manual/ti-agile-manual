%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                                       %%
%% Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com %%
%%                                                                       %%
%% Author: Chase Maupin <chase.maupin@ti.com>                            %%
%%                                                                       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Life Cycle of a Story}
\label{chap:life-cycle-of-story}

\paragraph{}
This chapter focuses on how a Story (or PBI) progresses through a sprint
and the VersionOne tool used by the LCPD team.  The goal of this chapter
is to help user's know how they will move their stories from creation to
Done within the tool.

\begin{enumerate}
    \item The initial story is created within VersionOne.  In VersionOne
          stories are called \textbf{Backlog Items}.  These backlog items,
          or \textbf{PBIs}, can be created in multiple locations in the tool
          and can be added by most project members at any time in the project
          life cycle.  For example:
        \begin{itemize}
            \item A \textbf{Product Owner} may add PBIs to the list as
                  they receive them from the \textbf{Stake Holders}
            \item A \textbf{Scrum Team Member} may add a PBI to the list
                  during their development to track additional work found
                  that is not part of the current story or to track future
                  work.
            \item A \textbf{Scrum Team Member} may add a PBI to the list
                  as part of the backlog refinement or sprint planning if
                  an existing item is found to be too large and should be
                  split.
        \end{itemize}
          In all of the above cases this is usually done by clicking the
          \textit{Add Backlog Item Inline or Add Backlog Item} button.
    \item During the creation of a PBI the following information should be
          captured:
        \begin{itemize}
            \item[\textbf{Title:}] This is the short description of the PBI
                 which will be seen in the backlog view.
            \item[\textbf{Team:}] At PBI creation this should be the functional
                 team that the PBI belongs to (e.g. Connectivity or System
                 Integration)
            \item[\textbf{Feature Group:}] The device that the PBI applies to.
                 For generic PBIs this should be \textit{ALL}.
            \item[\textbf{Epic:}] The Epic this PBI belongs to if any.  New
                 epics should not be created, but instead PBI should be added
                 within the existing epics.  If you feel you need a new Epic
                 please work with your scrum master to add it.  For generic
                 PBIs there are generic top-level epics, while for device
                 specific PBIs there are device specific epics.
            \item[\textbf{Description:}] This should contain the details about
                 the backlog item such as current status, any work already
                 available, and \textbf{most importantly Acceptance Criteria}.
            \item[\textbf{Estimate:}] This should only be added if an item is
                 being created during a \textbf{Sprint Planning or Backlog
                 Refinement} meeting.  If not this should be left blank so it
                 can be estimated by the team later.
            \item[\textbf{Owners:}] This should only be added when a backlog
                 item is being added by a specific developer and they are
                 planning this item for themselves.
            \item[\textbf{Priority:}] This should only be set by the
                 \textit{Product Owner} if they are creating the item.  All
                 other times this should be left blank for the Product Owner
                 to assign.
        \end{itemize}
    \item Now that the PBI has been created it likely needs prioritization.
          This is the role of the \textbf{Product Owner} and they should be
          reviewing the backlog periodically to prioritize items without a
          priority set.
    \item With a prioritized backlog, and the assumption that our new PBI is
          the highest priority, we can now estimate the size of the backlog
          item if it has not already been done.  Many times this will be
          done as part of the functional team meeting described in section
          \ref{sec:functionalteammeeting}, but if not will be done during
          the first part of the sprint planning.
    \item We now have enough information to start planning our PBI for the
          sprint.  During sprint planning the sprint team will check the
          estimate.  If they believe the item is too large then they should
          break it down into smaller PBIs and repeat the above process for
          each.  Once they believe the PBI can be completed within the
          current sprint they should:
        \begin{itemize}
            \item Change the \textbf{Team} value to their scrum team.  This
                  will be important to help filter the task board view for
                  only their items as we will discuss later.
            \item Add it to the sprint backlog.  This can be done by editing
                  the item and assigning a value in the \textbf{Sprint} field
                  or dragging the PBI from the release backlog to the desired
                  sprint withing the \textbf{Sprint Planning} view.
        \end{itemize}
    \item With the PBI added to the sprint it is now time to break that item
          down into tasks as part of the second sprint planning meeting.
          There are many ways this can be done but the most common is to use
          the \textbf{Taskboard} view in the \textbf{Sprint Tracking} tab to
          see the current PBIs within the sprint.  You can filter the PBIs to
          only those signed up to by your team using the \textit{Team}
          drop-down.  To plan an PBI you can click the small arrow in the
          upper-right corner and selecting either:
        \begin{itemize}
            \item[\textbf{Add Task:}] to add a new task for the PBI.  You should
                 fill in the \textit{Detail Estimate} field with the number of
                 hours the task should take.  Keep in mind that a task should
                 no more than 1 day of effort.
            \item[\textbf{Add Test:}] to add a new set test item which will be
                 displayed in the \textbf{Testboard} view.  These tests will
                 allow you to track progress for the completion of the PBI and
                 also give the system test team a view of where they can assist
                 other team.  The dialog for the test task includes fields
                 to contain setup, steps, expected results, etc.
        \end{itemize}
          You should break down your PBIs as much as possible during the sprint
          planning meeting, but new tasks can be added over time.
    \item For each task you should give an estimate of the number of
          \textbf{hours} that task will require.  This is to allow you to
          track the ToDo progress during your sprint.  Tasks are estimated
          in hours vs story points because they should be able to be done
          in a single day.
    \item You are now ready to start the sprint and begin working on your
          tasks.  The first thing you should do is decide which task you want
          to start working on and using the drop down from the arrow in the
          upper-right select \textbf{Sign Me Up}.  You should then move that
          task to \textbf{In Progress} so that other know it is already
          being worked.  You can do this by dragging and dropping the task
          to the In Progress Column.  You can do this for as many tasks as
          you will be working on that day.
    \item At the end of each day (or at least before your daily scrum meeting)
          you should make sure to update the \textbf{taskboard}.  This
          involves:
        \begin{itemize}
            \item Moving the tasks into the appropriate column of the
                  taskboard.
            \item Updating the hours worked and ToDo on a task.  This can
                  be done by selecting the ``Track'' option for the task and
                  putting the hours worked towards that task in the
                  \textbf{effort} field and updating the \textbf{ToDo} field
                  to subtract those hours.
        \end{itemize}
    \item At the end of each day (or at least before your daily scrum meeting)
          you should make sure to update the \textbf{storyboard}.  This
          involves:
        \begin{itemize}
            \item If work has begun on a story then the story should be
                  moved to the \textbf{In Progress} state.
            \item If all the tasks and tests for a story are done then the
                  story should be moved to the \textbf{Done} state.
        \end{itemize}
          \textbf{NOTE:} The \textit{Accepted} status is for stories that
          are closed because the Product Owner has accepted them as closed.
          Stories will be put in this state during the sprint review.
    \item At the end of each day (or at least before your daily scrum meeting)
          you should make sure to update the \textbf{testboard}.  This
          involves moving the test to either the \textbf{Passed} or
          \textbf{Failed} columns if the test has been done.
    \item Finally, during the Sprint Review the product owner will determine
          which items are truly done and which are not.  When an item is
          determined to be done then it can be \textbf{Closed} which will move
          it to the \textit{Accepted} state and close all the tasks within
          that backlog item as well.
    \item For any items that had an \textbf{Upstream} component there should
          be the following stories created:
        \begin{itemize}
            \item A story should be created to address maintainer feedback.
                  ideally the patches have been submitted early enough that
                  you will know if there will be maintainer feedback prior
                  to the next sprint planning.
            \item A story must be created indicating the \textbf{Merged} item.
                  This story should be added to the backlog and assigned into
                  the proper epic to allow tracking of the merge of the patches
                  into the upstream tree.
        \end{itemize} 
\end{enumerate}

\paragraph{}
This concludes the life cycle of a story in VersionOne.  If you have additional
questions please reach out to your scrum master for help.
